# o3d_1-12_bull_3_twoboxclasses

An extended 2nd base project for CMU students studying Game Engine Development.

This one combines Ogre3d (1.12) and Bullet (3.0), title is a bit misleading. It creates another box but rather than a function with all the setup code this has been moved to an object. The code to update etc. also, this is a stepping stone towards creating a player class for our game. 
